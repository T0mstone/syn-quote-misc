use proc_macro2::TokenStream;

/// A wrapper that parses and prints tuples and `Vec`s and also prints arrays and slices.
///
/// Tuples are implemented up to length 4, or up to length 32 with the `seq-tuples-32` crate feature.
#[repr(transparent)]
#[derive(Debug, Default, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct Seq<X: ?Sized>(pub X);

impl<X: ?Sized> Seq<X> {
	/// Convert a reference of `X` into a reference of `Seq<X>`.
	pub fn wrap_ref(r: &X) -> &Self {
		// SAFETY: `Seq` is `repr(transparent)`
		unsafe { std::mem::transmute::<&X, &Seq<X>>(r) }
	}
}

#[cfg(feature = "parsing")]
const _PARSING: () = {
	use std::borrow::Cow;

	use syn::parse::{Parse, ParseStream};

	use crate::bor::Bor;

	macro_rules! impl_tuple {
		(@const [$e:expr] $($t:tt)*) => { $e };
		(@one $($T:ident)*) => {
			impl<$($T: Parse),*> Parse for Seq<($($T,)*)> {
				fn parse(_input: ParseStream) -> syn::Result<Self> {
					Ok(Self(($(impl_tuple!(@const [_input.parse()?] $T),)*)))
				}
			}
		};
		(@gen $($T:ident)* | ) => {
			impl_tuple!(@one $($T)*);
		};
		(@gen $($L:ident)* | $T:ident $($R:ident)*) => {
			impl_tuple!(@one $($L)*);
			impl_tuple!(@gen $($L)* $T | $($R)*);
		};
		($($T:ident)*) => {
			impl_tuple!(@gen | $($T)*);
		};
	}

	#[cfg(not(feature = "seq-tuples-32"))]
	impl_tuple!(A B C D);
	#[cfg(feature = "seq-tuples-32")]
	impl_tuple!(
		A B C D  E F G H
		I J K L  M N O P
		Q R S T  U V W X
		Y Z Γ Δ  Θ Λ Ξ Π
	);

	impl<T: Parse> Parse for Seq<Vec<T>> {
		fn parse(input: ParseStream) -> syn::Result<Self> {
			let mut res = vec![];
			while !input.is_empty() {
				res.push(input.parse()?);
			}
			Ok(Self(res))
		}
	}

	impl<'a, T: Clone + Parse> Parse for Seq<Cow<'a, [T]>> {
		fn parse(input: ParseStream) -> syn::Result<Self> {
			Seq::<Vec<T>>::parse(input).map(|Seq(v)| Seq(Cow::Owned(v)))
		}
	}

	impl<'a, T: Parse> Parse for Seq<Bor<'a, [T], Vec<T>>> {
		fn parse(input: ParseStream) -> syn::Result<Self> {
			Seq::<Vec<T>>::parse(input).map(|Seq(v)| Seq(Bor::Owned(v)))
		}
	}
};

#[cfg(feature = "printing")]
const _PRINTING: () = {
	use std::borrow::Cow;

	use quote::ToTokens;

	use crate::bor::Bor;

	macro_rules! impl_tuple {
		(@const [$e:expr] $($t:tt)*) => { $e };
		(@one $($T:ident)*) => {
			impl<$($T: ToTokens),*> ToTokens for Seq<($($T,)*)> {
				#[allow(non_snake_case)]
				fn to_tokens(&self, _tokens: &mut TokenStream) {
					let Self(($($T,)*)) = self;
					$($T.to_tokens(_tokens);)*
				}
			}
		};
		(@gen $($T:ident)* | ) => {
			impl_tuple!(@one $($T)*);
		};
		(@gen $($L:ident)* | $T:ident $($R:ident)*) => {
			impl_tuple!(@one $($L)*);
			impl_tuple!(@gen $($L)* $T | $($R)*);
		};
		($($T:ident)*) => {
			impl_tuple!(@gen | $($T)*);
		};
	}

	#[cfg(not(feature = "seq-tuples-32"))]
	impl_tuple!(A B C D);
	#[cfg(feature = "seq-tuples-32")]
	impl_tuple!(
		A B C D  E F G H
		I J K L  M N O P
		Q R S T  U V W X
		Y Z Γ Δ  Θ Λ Ξ Π
	);

	impl<'a, T: ToTokens> ToTokens for Seq<&'a [T]> {
		fn to_tokens(&self, tokens: &mut TokenStream) {
			for t in self.0 {
				t.to_tokens(tokens)
			}
		}
	}

	impl<T: ToTokens> ToTokens for Seq<[T]> {
		#[inline]
		fn to_tokens(&self, tokens: &mut TokenStream) {
			Seq(&self.0).to_tokens(tokens)
		}
	}

	impl<T: ToTokens> ToTokens for Seq<Vec<T>> {
		#[inline]
		fn to_tokens(&self, tokens: &mut TokenStream) {
			Seq(self.0.as_slice()).to_tokens(tokens)
		}
	}

	impl<T: ToTokens, const N: usize> ToTokens for Seq<[T; N]> {
		#[inline]
		fn to_tokens(&self, tokens: &mut TokenStream) {
			Seq(self.0.as_slice()).to_tokens(tokens)
		}
	}

	impl<'a, T: Clone + ToTokens> ToTokens for Seq<Cow<'a, [T]>> {
		fn to_tokens(&self, tokens: &mut TokenStream) {
			Seq(self.0.as_ref()).to_tokens(tokens)
		}
	}

	impl<'a, T: ToTokens> ToTokens for Seq<Bor<'a, [T], Vec<T>>> {
		fn to_tokens(&self, tokens: &mut TokenStream) {
			Seq(self.0.as_ref()).to_tokens(tokens)
		}
	}
};
