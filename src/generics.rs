//! Tools for working with generics.

use proc_macro2::{Ident, TokenStream};
use syn::punctuated::Punctuated;
use syn::{GenericParam, Generics, Lifetime, LifetimeParam, Token, TypeParam, WhereClause};

/// A type representing generic parameters ([`Generics`] without the where clause).
///
/// Not having the where clause enables this type to actually be parseable and printable.
#[cfg_attr(feature = "clone-impls", derive(Clone))]
#[cfg_attr(feature = "extra-traits", derive(Debug, Eq, PartialEq, Hash))]
pub struct GenericParams {
	pub lt_token: Token![<],
	pub params: Punctuated<GenericParam, Token![,]>,
	pub gt_token: Token![>],
}

/// Construct [`Generics`] from optional [`GenericParams`] and an optional [`WhereClause`].
pub fn combine_to_generics(
	params: Option<GenericParams>,
	where_clause: Option<WhereClause>,
) -> Generics {
	let (lt_token, params, gt_token) = match params {
		Some(GenericParams {
			lt_token,
			params,
			gt_token,
		}) => (Some(lt_token), params, Some(gt_token)),
		None => (None, Default::default(), None),
	};

	Generics {
		lt_token,
		params,
		gt_token,
		where_clause,
	}
}

/// Split [`Generics`] into optional [`GenericParams`] and an optional [`WhereClause`].
pub fn split_generics(generics: Generics) -> (Option<GenericParams>, Option<WhereClause>) {
	let Generics {
		lt_token,
		params,
		gt_token,
		where_clause,
	} = generics;

	// poor man's try block
	let params = (|| {
		Some(GenericParams {
			lt_token: lt_token?,
			params,
			gt_token: gt_token?,
		})
	})();

	(params, where_clause)
}

/// Subyping variance of a generic parameter.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Variance {
	/// The full type is a subtype if the parameter is a subtype, e.g. a field or a function's return type.
	Covar,
	/// The full type is a subtype if the parameter is a supertype, e.g. a function parameter.
	Contravar,
	/// The full type is never a subtype, e.g. something that is both a function parameter and return type.
	Invar,
}

/// The kind of parameter supported by [`PhantomData`].
#[cfg_attr(feature = "clone-impls", derive(Clone))]
#[cfg_attr(feature = "extra-traits", derive(Debug, Eq, PartialEq, Hash))]
pub enum PhantomParam {
	Type(Ident),
	Lifetime(Lifetime),
}

impl TryFrom<GenericParam> for PhantomParam {
	type Error = ();

	fn try_from(value: GenericParam) -> Result<Self, Self::Error> {
		match value {
			GenericParam::Type(ty) => Ok(Self::Type(ty.ident)),
			GenericParam::Lifetime(lt) => Ok(Self::Lifetime(lt.lifetime)),
			GenericParam::Const(_) => Err(()),
		}
	}
}

impl From<TypeParam> for PhantomParam {
	fn from(value: TypeParam) -> Self {
		Self::Type(value.ident)
	}
}

impl From<LifetimeParam> for PhantomParam {
	fn from(value: LifetimeParam) -> Self {
		Self::Lifetime(value.lifetime)
	}
}

impl<'a> TryFrom<&'a GenericParam> for PhantomParam {
	type Error = ();

	fn try_from(value: &'a GenericParam) -> Result<Self, Self::Error> {
		match value {
			GenericParam::Type(ty) => Ok(Self::Type(ty.ident.clone())),
			GenericParam::Lifetime(lt) => Ok(Self::Lifetime(lt.lifetime.clone())),
			GenericParam::Const(_) => Err(()),
		}
	}
}

impl<'a> From<&'a TypeParam> for PhantomParam {
	fn from(value: &'a TypeParam) -> Self {
		Self::Type(value.ident.clone())
	}
}

impl<'a> From<&'a LifetimeParam> for PhantomParam {
	fn from(value: &'a LifetimeParam) -> Self {
		Self::Lifetime(value.lifetime.clone())
	}
}

/// A helper for generating the correct [`PhantomData`](std::marker::PhantomData) without worrying about it.
#[cfg_attr(feature = "clone-impls", derive(Clone))]
#[cfg_attr(feature = "extra-traits", derive(Debug, Eq, PartialEq, Hash))]
pub struct PhantomData {
	params: Vec<(Variance, PhantomParam)>,
}

impl PhantomData {
	/// Construct a new `PhantomData` containing all the given parameters with their respective variance.
	pub fn from_params(
		params: impl IntoIterator<Item = (Variance, impl TryInto<PhantomParam>)>,
	) -> Self {
		Self {
			params: params
				.into_iter()
				.filter_map(|(var, param)| Some((var, param.try_into().ok()?)))
				.collect(),
		}
	}

	/// Determine whether this `PhantomData` is empty (contains no parameters).
	pub fn is_empty(&self) -> bool {
		self.params.is_empty()
	}
}

#[cfg(feature = "parsing")]
const _PARSING: () = {
	use syn::parse::{Parse, ParseStream};

	impl Parse for GenericParams {
		fn parse(input: ParseStream) -> syn::Result<Self> {
			Ok(Self {
				lt_token: input.parse()?,
				params: punctuated_parse_terminated_until(input, |input| input.peek(Token![>]))?,
				gt_token: input.parse()?,
			})
		}
	}

	// modified from Punctuated::parse_terminated
	fn punctuated_parse_terminated_until<T: Parse, P: Parse + syn::token::Token>(
		input: ParseStream,
		mut stop: impl FnMut(ParseStream) -> bool,
	) -> syn::Result<Punctuated<T, P>> {
		let mut punctuated = Punctuated::new();

		loop {
			if input.is_empty() || stop(input) {
				break;
			}
			let value = input.parse()?;
			punctuated.push_value(value);
			if input.is_empty() || stop(input) {
				break;
			}
			let punct = input.parse()?;
			punctuated.push_punct(punct);
		}

		Ok(punctuated)
	}
};

#[cfg(feature = "printing")]
const _PRINTING: () = {
	use quote::ToTokens;

	use crate::print::to_tokens_with;
	use crate::to_tokens;

	fn param_var_type(param: &PhantomParam, var: Variance) -> impl ToTokens + '_ {
		let ty = to_tokens_with(move |tokens| match param {
			PhantomParam::Type(ty) => ty.to_tokens(tokens),
			PhantomParam::Lifetime(lt) => to_tokens!(tokens += { .& #lt .() }),
		});

		to_tokens_with(move |tokens| {
			// pointers to allow for unsized types
			match var {
				Variance::Covar => to_tokens!(tokens += { .*.const #ty }),
				Variance::Contravar => to_tokens!(tokens += { .fn.(.*.const #ty) }),
				Variance::Invar => to_tokens!(tokens += { .fn.(.*.const #ty) .-> .*.const #ty }),
			}
		})
	}

	impl ToTokens for GenericParams {
		fn to_tokens(&self, tokens: &mut TokenStream) {
			self.lt_token.to_tokens(tokens);
			self.params.to_tokens(tokens);
			self.gt_token.to_tokens(tokens);
		}
	}

	impl PhantomData {
		fn type_path() -> impl ToTokens {
			to_tokens_with(|tokens| {
				to_tokens! {
					tokens += {
						..{ ::core::marker::PhantomData }
					}
				}
			})
		}

		/// Get a printable value representing a `PhantomData` type.
		pub fn make_type(&self) -> impl ToTokens + '_ {
			let path = Self::type_path();

			to_tokens_with(move |tokens| {
				to_tokens! {
					tokens += {
						#path .< .( for((var, param) in &self.params => { #(param_var_type(param, *var)) } / { ., }) ) .>
					}
				}
			})
		}

		/// Get a printable value representing the (single possible) value of a `PhantomData` type.
		#[inline]
		pub fn make_value(&self) -> impl ToTokens + '_ {
			Self::type_path()
		}

		/// Get a printable value representing a `PhantomData` type, or `None` if it would contain no parameters.
		pub fn make_opt_type(&self) -> Option<impl ToTokens + '_> {
			(!self.is_empty()).then(|| self.make_type())
		}

		/// Get a printable value representing the (single possible) value of a `PhantomData` type, or `None` if it would contain no parameters.
		pub fn make_opt_value(&self) -> Option<impl ToTokens + '_> {
			(!self.is_empty()).then(|| self.make_value())
		}
	}
};
