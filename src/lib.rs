//! A collection of small tools to make working with `syn`/`quote` a bit smoother.
//!
//! # Crate features
//! - `clone-impls` (*enabled by default*): Clone impls for all types.
//! - `extra-traits`: Debug, Eq, PartialEq, Hash impls for most types (Debug for all).
//! - `parsing` (*enabled by default*): Enables features related to parsing.
//! - `printing` (*enabled by default*): Enables features related to printing (and enables the `quote` dependency).
//! - `seq-tuples-32`: Implements traits for [`Seq`] of tuples up to length 32 instead of 4.
//! - `attrs`: enables the [`attrs`] module and features in it (and enables `syn/derive` to enable the `Attribute` type).
//! - `generics`: enables the [`generics`] module and features in it (and enables `syn/derive` to enable the necessary types).
//! - `trait-seal`: enables the [`print::trait_seal`] module and features in it (and enables `syn/derive` to enable the `Generics` type).
//! - `marker-type`: enables the [`print::marker_type`] module and features in it (and enables `syn/derive` to enable necessary types).
//! - `full`: All of the above.

pub use seq::Seq;

pub mod bor;

mod seq;
#[cfg(feature = "attrs")]
pub mod attrs;
#[cfg(feature = "generics")]
pub mod generics;

#[cfg(feature = "printing")]
pub mod print;

/// A simple wrapper that `panic!`s if the input is not a valid `T`.
fn parse2_unwrap_inner<T: syn::parse::Parse>(tokens: proc_macro2::TokenStream) -> T {
	match syn::parse2(tokens) {
		Ok(t) => t,
		Err(err) => panic!("{}", err),
	}
}
#[cfg(all(feature = "parsing", not(feature = "printing")))]
pub use parse2_unwrap_inner as parse2_unwrap;
#[cfg(all(feature = "parsing", feature = "printing"))]
#[inline]
/// A simple wrapper that `panic!`s if the input is not a valid `T`.
pub fn parse2_unwrap<T: syn::parse::Parse>(tokens: impl quote::ToTokens) -> T {
	parse2_unwrap_inner(tokens.to_token_stream())
}

// todo: OptByFirst? Or something more general? -> OptByPeek?
