//! The common pattern of a [trait seal](https://rust-lang.github.io/api-guidelines/future-proofing.html#sealed-traits-protect-against-downstream-implementations-c-sealed):
//! ```no_run
//! mod private {
//!     pub trait Sealed {}
//! }
//! ```

use proc_macro2::{Ident, Span, TokenStream};
use quote::ToTokens;
use syn::Generics;

use crate::bor::Bor;
use crate::print::to_tokens_with;

/// A type that prints as a trait seal.
#[cfg_attr(feature = "clone-impls", derive(Clone))]
#[cfg_attr(feature = "extra-traits", derive(Debug))]
pub struct TraitSeal<'a, T: ToTokens> {
	/// The name of the module containing the seal.
	pub private: Bor<'a, Ident>,
	/// The name of the seal.
	pub sealed: Bor<'a, Ident>,
	/// The `impl`s to give the sealed trait.
	pub impls_for: Vec<(Bor<'a, Generics>, T)>,
}

/// The default names are `mod private` and `pub trait Sealed`.
impl<'a, T: ToTokens> Default for TraitSeal<'a, T> {
	fn default() -> Self {
		Self::new_call_site("private", "Sealed")
	}
}

impl<'a, T: ToTokens> TraitSeal<'a, T> {
	/// Construct a new trait seal with the given names (spanned at the call site) and no `impl`s (yet).
	pub fn new_call_site(private: &str, sealed: &str) -> Self {
		Self {
			private: Bor::Owned(Ident::new(private, Span::call_site())),
			sealed: Bor::Owned(Ident::new(sealed, Span::call_site())),
			impls_for: Vec::new(),
		}
	}

	/// Add an `impl` for the trait seal.
	pub fn add_impl(&mut self, generics: Bor<'a, Generics>, ty: T) {
		self.impls_for.push((generics, ty));
	}
}

impl<'a, T: ToTokens> ToTokens for TraitSeal<'a, T> {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		let Self {
			private,
			sealed,
			impls_for,
		} = self;

		let impls = to_tokens_with(|tokens| {
			for (g, ty) in impls_for {
				let (gi, _, wh) = g.split_for_impl();

				to_tokens! {
					tokens += {
						.impl #gi #sealed .for #ty #wh .{}
					}
				}
			}
		});

		to_tokens! {
			tokens += {
				.mod #private .{
					.pub .trait #sealed .{}

					#impls
				}
			}
		}
	}
}
