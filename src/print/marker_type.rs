//! A module containing utilities to construct a marker type.

use proc_macro2::{Ident, TokenStream};
use quote::ToTokens;
use syn::{Attribute, Generics, Visibility};

use crate::bor::Bor;
use crate::generics::{PhantomData, Variance};
use crate::print::to_tokens_with;
use crate::Seq;

/// A type describing the single possible value of a marker type.
#[cfg_attr(feature = "clone-impls", derive(Clone))]
#[cfg_attr(feature = "extra-traits", derive(Debug))]
pub struct MarkerTypeValue<'a> {
	/// A visibility describing the scope from which the type can be constructed.
	pub vis: Bor<'a, Visibility>,
	/// Whether to implement [`Default`] for the type.
	pub impl_default: bool,
}

/// A type describing a marker type.
#[cfg_attr(feature = "clone-impls", derive(Clone))]
#[cfg_attr(feature = "extra-traits", derive(Debug))]
pub struct MarkerType<'a> {
	/// The attributes attached to the type.
	pub attrs: Seq<Bor<'a, [Attribute], Vec<Attribute>>>,
	/// The visibility of the type.
	pub vis: Bor<'a, Visibility>,
	/// The name of the type.
	pub ident: Bor<'a, Ident>,
	pub generics: Bor<'a, Generics>,
	/// The possible value of the marker type:
	/// - `None` means that no value of the type can ever exist (like [`Infallible`](std::convert::Infallible))
	/// - `Some` means there should be exactly one possible value of the type, and contains additional info regarding it.
	///
	/// If there are no generic parameters, a value of `Some` with a visibility of [`Public`](Visibility::Public) will generate a unit struct.
	pub possible_value: Option<MarkerTypeValue<'a>>,
}

impl<'a> ToTokens for MarkerType<'a> {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		let Self {
			attrs,
			vis,
			ident,
			generics,
			possible_value,
		} = self;

		let pd = PhantomData::from_params(generics.params.iter().map(|p| (Variance::Invar, p)));

		let opt_params = to_tokens_with(|tokens| {
			if !generics.params.is_empty() {
				to_tokens!(tokens += { .< #(generics.params) .> });
			}
		});

		match possible_value {
			None => {
				let ty = pd.make_opt_type();

				to_tokens! {
					tokens += {
						#attrs
						#vis .struct #ident #opt_params .(
							if(let Some(ty) = ty => { #ty ., })
							..{ ::core::convert::Infallible }
						) #(generics.where_clause) .;
					}
				}
			}
			Some(MarkerTypeValue {
				vis: fvis,
				impl_default,
			}) => {
				let fvis = fvis.as_ref();

				let opt_field_ty = to_tokens_with(|tokens| match pd.make_opt_type() {
					Some(ty) => to_tokens!(tokens += { .(#fvis #ty) }),
					None if matches!(fvis, Visibility::Public(_)) => (),
					None => to_tokens!(tokens += { .(#fvis .()) }),
				});
				let opt_field_value = to_tokens_with(|tokens| match pd.make_opt_value() {
					Some(val) => to_tokens!(tokens += { .(#val) }),
					None if matches!(fvis, Visibility::Public(_)) => (),
					None => to_tokens!(tokens += { .(.()) }),
				});

				to_tokens! {
					tokens += {
						#attrs
						#vis .struct #ident #opt_params #opt_field_ty
						#(generics.where_clause) .;
					}
				}

				if *impl_default {
					let (impl_generics, arg_generics, where_clause) = generics.split_for_impl();

					to_tokens! {
						tokens += {
							.impl #impl_generics ..{ ::core::default::Default } .for #ident #arg_generics
							#where_clause
							.{
								.fn ..{ default() -> } .Self .{
									.Self #opt_field_value
								}
							}
						}
					}
				}
			}
		}
	}
}
