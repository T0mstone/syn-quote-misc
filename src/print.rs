//! A module containing print-only utilities.

use proc_macro2::TokenStream;
use quote::ToTokens;

/// Not public API
#[doc(hidden)]
#[macro_export]
macro_rules! _to_tokens {
	(@one $target:ident # $var:ident) => {
		::quote::ToTokens::to_tokens(&$var, $target);
	};
    (@one $target:ident # ($e:expr)) => {
		match $e {
			ref _e => ::quote::ToTokens::to_tokens(_e, $target)
		}
	};
	(@one $target:ident # $b:block) => {
		match $b {
			ref _e => ::quote::ToTokens::to_tokens(_e, $target)
		}
	};
	(@one $target:ident @ $i:ident) => {
		::quote::ToTokens::to_tokens(&::proc_macro2::Ident::new(stringify!($i), ::proc_macro2::Span::call_site()), $target);
	};
	(@one $target:ident @ ($e:expr)) => {
		::quote::ToTokens::to_tokens(&::proc_macro2::Ident::new($e, ::proc_macro2::Span::call_site()), $target);
	};
	(@one $target:ident . ( $($t:tt)* )) => {
		::syn::token::Paren(::proc_macro2::Span::call_site()).surround($target, |_inner| { $crate::to_tokens!(_inner += { $($t)* }); });
	};
	(@one $target:ident . [ $($t:tt)* ]) => {
		::syn::token::Bracket(::proc_macro2::Span::call_site()).surround($target, |_inner| { $crate::to_tokens!(_inner += { $($t)* }); });
	};
	(@one $target:ident . { $($t:tt)* }) => {
		::syn::token::Brace(::proc_macro2::Span::call_site()).surround($target, |_inner| { $crate::to_tokens!(_inner += { $($t)* }); });
	};
	(@one $target:ident . $tok:tt) => {
		::quote::ToTokens::to_tokens(&::syn::Token![$tok](::proc_macro2::Span::call_site()), $target);
	};
	(@one $target:ident .. { $($t:tt)* }) => {
		$($crate::_to_tokens!(@tok $target $t);)*
	};
	(@one $target:ident if ( let $p:pat = $cond:expr => $then:tt $(else $els:tt)? )) => {
		if let $p = $cond {
			$crate::to_tokens!($target += $then);
		} $(else {
			$crate::to_tokens!($target += $els);
		})?
	};
	(@one $target:ident if ( $cond:expr => $then:tt $(else $els:tt)? )) => {
		if $cond {
			$crate::to_tokens!($target += $then);
		} $(else {
			$crate::to_tokens!($target += $els);
		})?
	};
	(@one $target:ident for ( $p:pat in $iter:expr => $each:tt $(/ $sep:tt)? )) => {
		// if there is no sep, this declaration is optimized away
		let mut _first = true;
		for $p in $iter {
			$(if _first {
				_first = false;
			} else {
				$crate::to_tokens!($target += $sep);
			})?
			$crate::to_tokens!($target += $each);
		}
	};
	(@one $target:ident fn ($toks:ident => $f:expr)) => {
		{
			let $toks = &mut *$target;
			$f;
		}
	};

	(@tok $target:ident $i:ident) => {
		$crate::_to_tokens!(@one $target @ $i);
	};
	(@tok $target:ident ($($t:tt)*)) => {
		$crate::_to_tokens!(@one $target . ( ..{$($t)*} ));
	};
	(@tok $target:ident [$($t:tt)*]) => {
		$crate::_to_tokens!(@one $target . [ ..{$($t)*} ]);
	};
	(@tok $target:ident {$($t:tt)*}) => {
		$crate::_to_tokens!(@one $target . { ..{$($t)*} });
	};
	(@tok $target:ident $t:tt) => {
		$crate::_to_tokens!(@one $target . $t);
	};
}

/// A macro similar to [`quote::quote`], with two major differences:
/// 1. it expands directly to either [`to_tokens`](ToTokens::to_tokens) or [`to_tokens_with`] instead of constructing its own [`TokenStream`].
/// 2. it is a lot simpler and has quite different (less elegant) syntax
///
/// It is primarily useful for implementing [`ToTokens`] without allocating a new [`TokenStream`] every time (as you would do with [`quote::quote`]).
/// This explains the first difference; The second one is more of an implementation decision to keep it relatively simple.
///
/// ## Syntax
/// There are two entrypoints to the syntax:
/// - `to_tokens!(move? || { <...> })` expands to [`to_tokens_with`].
/// - `to_tokens!(<ident> += { <...> })` expands to [`to_tokens`](ToTokens::to_tokens)`(<...>, <ident>)` (so `<ident>` needs to have type `&mut `[`TokenStream`]).
///
/// The syntax for the content (`<...>`) is the same in both cases:
/// - `#<ident>`: like `quote`, emits the variable of that name using [`ToTokens`].
/// - `#(<expr>)`, `#<block>`: emits the value of the expr using [`ToTokens`].
/// - `@<ident>`: emits that identifier using the call site as the span.
/// - `@(<expr>)`: emits an identifier, whose name is given by the expr, using the call site as the span (the expr needs to have type `&str`).
/// - `.<token>`: emits a single token literally.
/// - `.. { <tokens> }`: emits a sequence of tokens literally.
/// - `.(<...>)` emits `<...>` (which still uses this syntax) surrounded by parentheses.
/// - `.[<...>]` emits `<...>` (which still uses this syntax) surrounded by square brackets.
/// - `.{<...>}` emits `<...>` (which still uses this syntax) surrounded by curly braces.
///
/// There are also some control flow constructs (with slightly inconvenient syntax) that work in a straight-forward manner:
/// - `if(<expr> => { <...> } else { <...> })` (the `else` branch can be left out).
/// - `if(let <pat> = <expr> => { <...> } else { <...> })` (the `else` branch can be left out).
/// - `for(<pat> in <expr> => { <...> })`
/// - `for(<pat> in <expr> => { <...> } / { <...> })`: The second `<...>` is a separator that is emitted inbetween the expansions of the main body.
///
/// And if you need to put custom code inside this, you can use:
/// - `fn(<ident> => <expr>)`: Binds `<ident>` to a mutable reference of the output `TokenStream` and lets you mutate it in `<expr>`.
#[macro_export]
macro_rules! to_tokens {
	(move || { $($t:tt)* }) => {
		$crate::print::to_tokens_with(move |tokens| $crate::to_tokens!(tokens += { $($t)* }))
	};
	(|| { $($t:tt)* }) => {
		$crate::print::to_tokens_with(|tokens| $crate::to_tokens!(tokens += { $($t)* }))
	};
    ($target:ident += { $($kind:tt $tok:tt)* }) => {
		{ $($crate::_to_tokens!(@one $target $kind $tok);)* }
	};
}

/// A wrapper for a function that prints itself by calling that function.
#[repr(transparent)]
#[derive(Default, Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct ToTokensWith<F>(F);

/// Create an ad-hoc value that implements [`ToTokens`] by using the given function as [`to_tokens`](ToTokens::to_tokens).
#[inline]
pub fn to_tokens_with<F>(f: F) -> ToTokensWith<F>
where
	F: Fn(&mut TokenStream),
{
	ToTokensWith(f)
}

impl<F: Fn(&mut TokenStream)> ToTokens for ToTokensWith<F> {
	#[inline]
	fn to_tokens(&self, tokens: &mut TokenStream) {
		(self.0)(tokens);
	}
}

#[cfg(feature = "trait-seal")]
pub mod trait_seal;
#[cfg(feature = "marker-type")]
pub mod marker_type;
