//! Borrow-on-read data.

use std::fmt::Debug;
use std::hash::{Hash, Hasher};
use std::ops::Deref;

use proc_macro2::TokenStream;

/// Similar to [`AsRef`], but [reflexive](AsRef#reflexivity).
pub trait ReflexiveAsRef<T: ?Sized> {
	fn as_ref(&self) -> &T;
}

impl<T: ?Sized> ReflexiveAsRef<T> for T {
	#[inline]
	fn as_ref(&self) -> &T {
		self
	}
}

impl<T> ReflexiveAsRef<[T]> for Vec<T> {
	fn as_ref(&self) -> &[T] {
		&self[..]
	}
}

/// Borrow-on-read, the inverse of [clone-on-write](std::borrow::Cow).
///
/// This type is mainly useful for storing data that is then only used by reference.
#[derive(Debug)]
pub enum Bor<'a, T: ?Sized, O: ReflexiveAsRef<T> = T> {
	Borrowed(&'a T),
	Owned(O),
}

impl<'a, T: ?Sized, O: ReflexiveAsRef<T>> Bor<'a, T, O> {
	// inherent function to make usage easier due to method resolution precedence
	fn as_ref(&self) -> &T {
		match self {
			Bor::Borrowed(t) => t,
			Bor::Owned(o) => o.as_ref(),
		}
	}
}

impl<'a, T: ?Sized, O: ReflexiveAsRef<T>> Clone for Bor<'a, T, O>
where
	O: Clone,
{
	fn clone(&self) -> Self {
		match self {
			Self::Borrowed(r) => Self::Borrowed(*r),
			Self::Owned(o) => Self::Owned(o.clone()),
		}
	}
}

impl<'a, T: ?Sized, O: ReflexiveAsRef<T>> Copy for Bor<'a, T, O> where O: Copy {}

impl<'a, T: ?Sized, O: ReflexiveAsRef<T>> PartialEq for Bor<'a, T, O>
where
	T: PartialEq,
{
	fn eq(&self, other: &Self) -> bool {
		self.as_ref() == other.as_ref()
	}
}

impl<'a, T: ?Sized, O: ReflexiveAsRef<T>> Eq for Bor<'a, T, O> where T: Eq {}

impl<'a, T: ?Sized, O: ReflexiveAsRef<T>> Hash for Bor<'a, T, O>
where
	T: Hash,
{
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.as_ref().hash(state);
	}
}

impl<'a, T: ?Sized, O: ReflexiveAsRef<T>> ReflexiveAsRef<T> for Bor<'a, T, O> {
	#[inline]
	fn as_ref(&self) -> &T {
		self.as_ref()
	}
}

impl<'a, T: ?Sized, O: ReflexiveAsRef<T>> AsRef<T> for Bor<'a, T, O> {
	#[inline]
	fn as_ref(&self) -> &T {
		self.as_ref()
	}
}

impl<'a, T: ?Sized, O: ReflexiveAsRef<T>> Deref for Bor<'a, T, O> {
	type Target = T;

	#[inline]
	fn deref(&self) -> &Self::Target {
		self.as_ref()
	}
}

#[cfg(feature = "parsing")]
const _PARSING: () = {
	use syn::parse::{Parse, ParseStream};

	impl<'a, T: ?Sized, O: ReflexiveAsRef<T> + Parse> Parse for Bor<'a, T, O> {
		#[inline]
		fn parse(input: ParseStream) -> syn::Result<Self> {
			O::parse(input).map(Self::Owned)
		}
	}
};

#[cfg(feature = "printing")]
const _PRINTING: () = {
	use quote::ToTokens;

	impl<'a, T: ?Sized + ToTokens, O: ReflexiveAsRef<T>> ToTokens for Bor<'a, T, O> {
		#[inline]
		fn to_tokens(&self, tokens: &mut TokenStream) {
			self.as_ref().to_tokens(tokens)
		}
	}
};
