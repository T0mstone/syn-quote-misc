//! Wrappers for parsing attributes.

use proc_macro2::TokenStream;
use syn::Attribute;

use crate::seq::Seq;

/// A wrapper that parses using [`Attribute::parse_inner`] and prints them using [`Seq`].
#[cfg_attr(feature = "clone-impls", derive(Clone))]
#[cfg_attr(feature = "extra-traits", derive(Debug, Eq, PartialEq, Hash))]
pub struct InnerAttributes(pub Vec<Attribute>);

/// A wrapper that parses using [`Attribute::parse_outer`] and prints them using [`Seq`].
#[cfg_attr(feature = "clone-impls", derive(Clone))]
#[cfg_attr(feature = "extra-traits", derive(Debug, Eq, PartialEq, Hash))]
pub struct OuterAttributes(pub Vec<Attribute>);

#[cfg(feature = "parsing")]
const _PARSING: () = {
	use syn::parse::{Parse, ParseStream};

	impl Parse for InnerAttributes {
		fn parse(input: ParseStream) -> syn::Result<Self> {
			Ok(Self(Attribute::parse_inner(input)?))
		}
	}

	impl Parse for OuterAttributes {
		fn parse(input: ParseStream) -> syn::Result<Self> {
			Ok(Self(Attribute::parse_outer(input)?))
		}
	}
};

#[cfg(feature = "printing")]
const _PRINTING: () = {
	use quote::ToTokens;

	impl ToTokens for InnerAttributes {
		fn to_tokens(&self, tokens: &mut TokenStream) {
			Seq(self.0.as_slice()).to_tokens(tokens)
		}
	}

	impl ToTokens for OuterAttributes {
		fn to_tokens(&self, tokens: &mut TokenStream) {
			Seq(self.0.as_slice()).to_tokens(tokens)
		}
	}
};
